public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println("Says: " + dog.getName() + " says " + dog.speak());

        System.out.println();

        Labrador labrador = new Labrador("marley", "black");
        System.out.println(labrador);
        System.out.println("Says: " + labrador.speak());

        System.out.println();

        Yorkshire yorkshire = new Yorkshire("Anna");
        System.out.println(yorkshire);
        System.out.println("Says: " + yorkshire.speak());

        System.out.println();

        Cage cage = new Cage();
        cage.add(dog);
        cage.add(labrador);
        cage.add(yorkshire);
        System.out.println(cage);
        cage.chaos();
        cage.remove("Spike");
        System.out.println(cage);
    }
}
