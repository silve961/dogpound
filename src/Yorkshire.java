/**
 Yorkshire.java
 A class derived from Dog that holds information about
 a Yorkshire terrier. Overrides Dog speak method.
 @ TODO this file is largely incomplete

 */

public class Yorkshire extends Dog
{
    private static int breedWeight = 75;

    public Yorkshire(String name)
    {
        super(name);
    }

    /**
     * Small bark -- overrides speak method in Dog
     * @return a small bask string
     */

    public String speak()
    {
        return "bau bau bau";
    }

    public static int avgBreedWeight()
    {
        return breedWeight;
    }

    @Override
    public String toString() {
        return "Name: " + name + ", Height: " + breedWeight;

    }
}
